﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using inRiver.Integration.Import;
using inRiver.Integration.Reporting;
using System.Data;
using System.Data.OleDb;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace inRiverImport
{
    public class ExcelImporter : FileImport
    {
        public override void InitFileImportSettings()
        {
            throw new NotImplementedException();
        }

        public override void ProcessFile(string filePath)
        {
            ReportManager.Instance.Write(Id, "just started in: " + filePath);

            string fileName = System.IO.Path.GetFileName(filePath);

            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                string text;
                string rowText = "";

                //ReportManager.Instance.Write(Id, "row ct:" + sheetData.Elements<Row>().Count());
                foreach (Row r in sheetData.Elements<Row>())
                {
                    foreach (Cell c in r.Elements<Cell>())
                    {
                        text = GetCellValue(c, workbookPart);
                        rowText += ":"+text;
                    }
                    ReportManager.Instance.Write(Id, "combined cell value:" + rowText);
                }
            }

            //ReadExcelFileDOM(fileName);    // DOM


            /*
            //DataSet ds = GetDataSetFromExcelFile(fileName);
            DataSet ds = new DataSet();

            //string connectionString = "Provider=Microsoft.Jet.OleDb.4.0; Data Source=" + fileName +  "; Extended Properties = Excel 8.0; ";
            //string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName + ";Extended Properties=\"Excel 12.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text\""; ;

            string connectionString = GetConnectionString(fileName);

            try
            {
                using (var conn = new OleDbConnection(connectionString))
                {
                    conn.Open();
                    OleDbCommand cmd = new OleDbCommand();
                    cmd.Connection = conn;
                    DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    ReportManager.Instance.Write(Id, "dt count" + dtSheet.Rows.Count);
                    // Loop through all Sheets to get data
                    foreach (DataRow dr in dtSheet.Rows)
                    {
                        string sheetName = dr["TABLE_NAME"].ToString();
                        ReportManager.Instance.Write(Id, "sheetName" + sheetName);
                        if (!sheetName.EndsWith("$"))
                            continue;

                        // Get all rows from the Sheet
                        cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                        DataTable dt = new DataTable();
                        dt.TableName = sheetName;

                        OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                        da.Fill(dt);

                        ds.Tables.Add(dt);
                    }

                    cmd = null;
                    conn.Close();
                }
            }
            catch (Exception e)
            {
                ReportManager.Instance.Write(Id, "excep" + e.Message);
            }

            foreach (DataTable table in ds.Tables)
            {
                ReportManager.Instance.Write(Id, "no of rows:"+table.Rows.Count);
                foreach (DataRow row in table.Rows)
                {
                    ReportManager.Instance.Write(Id, "no of cols:" + table.Columns.Count);
                    foreach (DataColumn column in table.Columns)
                    {
                        object item = row[column];
                        // read column and item
                        //Console.WriteLine("item :" + item.ToString());
                        ReportManager.Instance.Write(Id, "item__:" + item.ToString());
                    }
                }
            }
            */

            ReportManager.Instance.Write(Id, "File imported successfully");
        }

        static void ReadExcelFileDOM(string fileName)
        {

            using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(fileName, false))
            {
                WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;
                WorksheetPart worksheetPart = workbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();
                string text;

                //Console.WriteLine("row ct:" + sheetData.Elements<Row>().Count());
                //ReportManager.Instance.Write(Id, "row ct:" + sheetData.Elements<Row>().Count());
                foreach (Row r in sheetData.Elements<Row>())
                {
                    foreach (Cell c in r.Elements<Cell>())
                    {
                        text = GetCellValue(c, workbookPart);
                        //Console.Write(text + " ");
                        //ReportManager.Instance.Write(Id, "cell value:" + text);
                    }
                }
            }
        }


        public static string GetCellValue(Cell cell, WorkbookPart wbPart)
        {
            string value = null;

            if (cell != null)
            {
                value = cell.InnerText;
                if (cell.DataType != null)
                {
                    switch (cell.DataType.Value)
                    {
                        case CellValues.SharedString:
                            var stringTable =
                                wbPart.GetPartsOfType<SharedStringTablePart>()
                                .FirstOrDefault();
                            if (stringTable != null)
                            {
                                value =
                                    stringTable.SharedStringTable
                                    .ElementAt(int.Parse(value)).InnerText;
                            }
                            break;

                        case CellValues.Boolean:
                            switch (value)
                            {
                                case "0":
                                    value = "FALSE";
                                    break;
                                default:
                                    value = "TRUE";
                                    break;
                            }
                            break;
                    }
                }

            }
            return value;
        }



        private static string GetConnectionString(string file)
        {
            Dictionary<string, string> props = new Dictionary<string, string>();

            string extension = file.Split('.').Last();

            if (extension == "xls")
            {
                //Excel 2003 and Older
                props["Provider"] = "Microsoft.Jet.OLEDB.4.0";
                props["Extended Properties"] = "Excel 8.0";
            }
            else if (extension == "xlsx")
            {
                //Excel 2007, 2010, 2012, 2013
                props["Provider"] = "Microsoft.ACE.OLEDB.12.0;";
                props["Extended Properties"] = "Excel 12.0";
                //props["IMEX"] = "1";
                //props["HDR"] = "NO";
                //props["TypeGuessRows"] = "0";
                //props["ImportMixedTypes"] = "Text\""; 
            }
            else
                throw new Exception(string.Format("error file: {0}", file));

            props["Data Source"] = file;

            StringBuilder sb = new StringBuilder();

            foreach (KeyValuePair<string, string> prop in props)
            {
                sb.Append(prop.Key);
                sb.Append('=');
                sb.Append(prop.Value);
                sb.Append(';');
            }

            return sb.ToString();
        }


        private static DataSet GetDataSetFromExcelFile(string file)
        {
            DataSet ds = new DataSet();

            //string connectionString = "Provider=Microsoft.Jet.OleDb.4.0; Data Source=" + file +  "; Extended Properties = Excel 8.0; ";
            //string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + file + ";Extended Properties=\"Excel 12.0;IMEX=1;HDR=NO;TypeGuessRows=0;ImportMixedTypes=Text\""; ;

            string connectionString = GetConnectionString(file);

            using (var conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                OleDbCommand cmd = new OleDbCommand();
                cmd.Connection = conn;
                DataTable dtSheet = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                // Loop through all Sheets to get data
                foreach (DataRow dr in dtSheet.Rows)
                {
                    string sheetName = dr["TABLE_NAME"].ToString();
                    if (!sheetName.EndsWith("$"))
                        continue;

                    // Get all rows from the Sheet
                    cmd.CommandText = "SELECT * FROM [" + sheetName + "]";

                    DataTable dt = new DataTable();
                    dt.TableName = sheetName;

                    OleDbDataAdapter da = new OleDbDataAdapter(cmd);
                    da.Fill(dt);

                    ds.Tables.Add(dt);
                }

                cmd = null;
                conn.Close();
            }

            return ds;
        }

        public override bool ShouldProcessFile(string filePath)
        {
            return true;
            //throw new NotImplementedException();
        }
    }
}
